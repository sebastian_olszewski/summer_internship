package ControlFlowSandbox.src.exercises;

public class ex1PositiveNegativeZero {
    public static void main(String[] args)
    {
        System.out.println(checkNumber(123));
        System.out.println(checkNumber(5213));
        System.out.println(checkNumber(2));
        System.out.println(checkNumber(0));
        System.out.println(checkNumber(-123));
        System.out.println(checkNumber(0));
        System.out.println(checkNumber(-213));
    }
    public static String checkNumber(int number)
    {
        if(number > 0)
        {
            return "Positive";
        } else if (number == 0)
        {
            return "Zero";
        }else
        return "Negative";
    };
}
