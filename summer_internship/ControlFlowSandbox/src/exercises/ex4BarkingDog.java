package ControlFlowSandbox.src.exercises;

public class ex4BarkingDog {
    public static void main(String[] args) {
        System.out.println(shouldWakeUp(false, 4));
        System.out.println(shouldWakeUp(true, 4));
        System.out.println(shouldWakeUp(false, 12));
        System.out.println(shouldWakeUp(true, -2));
        System.out.println(shouldWakeUp(true, 26));
        System.out.println(shouldWakeUp(true, 3));


    }

    public static boolean shouldWakeUp(boolean isBarking, int hourOfDay)
    {
        if(isBarking == true && ((hourOfDay < 8 && hourOfDay >=0) || (hourOfDay > 22 && hourOfDay <= 23))) {
            return true;
        }
        return false;
    }
}
