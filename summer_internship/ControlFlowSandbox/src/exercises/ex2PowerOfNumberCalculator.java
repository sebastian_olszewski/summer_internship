package ControlFlowSandbox.src.exercises;

public class ex2PowerOfNumberCalculator {
    public static void main(String[] args) {

        System.out.println(calculatePower(2, 4) == 16);
        System.out.println(calculatePower(3, 9));
        System.out.println(calculatePower(-2, 9));
    }
    private static long calculatePower(int base, int power)
    {
        if(power < 0) return 0;
        if(power == 0) return 1;
        int result = 1;
        for (int i = 1; i <= power; i++) {
            result *= base;
        }
        return result;
    }
}
