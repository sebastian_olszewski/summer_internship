package ControlFlowSandbox.src.exercises;

public class ex2coma5DigitCounter {

    public static void main(String[] args) {
        System.out.println(numberOfDigits(-123) == -1);
        System.out.println(numberOfDigits(1) == 1);
        System.out.println(numberOfDigits(0) == 1);
        System.out.println(numberOfDigits(454523425) == 9);
    }

    private static int numberOfDigits(int targetNumber)
    {
        int counter = 0;
        if(targetNumber < 0) {
            return -1;
        }else if (targetNumber == 0){
            counter = 1;
            return counter;
        }else {
            while(targetNumber > 0)
            {
                targetNumber /= 10;
                counter += 1;
            }
            return counter;
        }

    }
}
