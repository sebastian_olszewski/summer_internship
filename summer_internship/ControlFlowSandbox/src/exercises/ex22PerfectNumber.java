package ControlFlowSandbox.src.exercises;

public class ex22PerfectNumber {
    public static void main(String[] args) {
        System.out.println(isPerfectNumber(6));
        System.out.println(isPerfectNumber(28));
        System.out.println(isPerfectNumber(5));
        System.out.println(isPerfectNumber(-1));
    }

    public static boolean isPerfectNumber(int number)
    {
        int divider = 1;
        int sum = 0;
        if (number < 1)
        {
            return false;
        }else
        {
            while(divider <= (number/2))
            {
                if(number % divider == 0)
                {
                    sum += divider;
                    divider++;
                }else
                {
                    divider++;
                }
            }
            if(sum == number)
            {
                return true;
            }else
            {
                return false;
            }
        }
    }
}
