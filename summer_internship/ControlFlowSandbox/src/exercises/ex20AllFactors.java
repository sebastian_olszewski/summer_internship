package ControlFlowSandbox.src.exercises;

public class ex20AllFactors {
    public static void main(String[] args) {
        printFactors(6);
        printFactors(32);
        printFactors(10);
        printFactors(-1);
        printFactors(948381849);
    }

    public static void printFactors(int number)
    {
        int divider = 2;
        String numbers = "1";
        if (number < 1)
        {
            System.out.println("Invalid Value");
        }else
        {
            while(divider <= (number / 2))
            {
                if(number % divider == 0)
                {
                    numbers += " " + divider;
                    divider++;
                }else
                {
                    divider++;
                }
            }
            System.out.println(numbers + " " + number);
        }
    }
}
