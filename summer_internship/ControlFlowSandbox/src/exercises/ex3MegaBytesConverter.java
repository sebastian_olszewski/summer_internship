package ControlFlowSandbox.src.exercises;

public class ex3MegaBytesConverter {
    public static void main(String[] args) {

        printMegaBytesAndKiloBytes(12312312);
        printMegaBytesAndKiloBytes(2500);
        printMegaBytesAndKiloBytes(5000);
        printMegaBytesAndKiloBytes(76324);
        printMegaBytesAndKiloBytes(0);
        printMegaBytesAndKiloBytes(-12321);
    }

    public static void printMegaBytesAndKiloBytes(int kiloBytes)
    {
        if (kiloBytes >= 0)
        {
            int Mb = kiloBytes/1024;
            int KB = kiloBytes - (Mb*1024);
            System.out.println(kiloBytes + " KB = " + Mb + " MB and " + KB + " KB");
        }else
        {
            System.out.println("Invalid number");
        }
    }
}