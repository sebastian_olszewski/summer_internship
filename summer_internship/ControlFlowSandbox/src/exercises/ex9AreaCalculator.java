package ControlFlowSandbox.src.exercises;
import java.lang.Math;

public class ex9AreaCalculator {
    public static void main(String[] args) {
        System.out.println(area(5));
        System.out.println(area(-1));
        System.out.println(area(5.0, 4.0));
        System.out.println(area(-1.0, 4.0));
    }

    private static double area(double radius)
    {
        if(radius < 0)
        {
            return -1;
        }

        return Math.PI*radius*radius;
    }

    private static double area(double x, double y)
    {
        if(x < 0 || y < 0)
        {
            return -1;
        }

        return x*y;
    }
}
