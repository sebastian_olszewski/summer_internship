package ControlFlowSandbox.src.exercises;

public class ex18SharedDigit {
    public static void main(String[] args) {
        System.out.println(hasSharedDigit(12, 23));
        System.out.println(hasSharedDigit(9, 99));
        System.out.println(hasSharedDigit(15, 55));
    }

    private static boolean hasSharedDigit(int num1, int num2) {
        int temp1 = num1 % 10;
        int temp2 = (num1 / 10);
        if (num1 <= 10 || num1 > 99 || num2 <= 10 || num2 > 99) {
            return false;
        } else if (temp1 == num2 % 10 || temp1 == (num2 / 10) || temp2 == num2 % 10 || temp2 == (num2 / 10)) {
            return true;
        } else
        {
            return false;
        }
    }
}
