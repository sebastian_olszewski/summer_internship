package ControlFlowSandbox.src.exercises;

public class ex6DecimalComparator {
    public static void main(String[] args) {
        System.out.println(areEqualByThreeDecimalPlaces(-3.1756, -3.175));
        System.out.println(areEqualByThreeDecimalPlaces(3.175, 3.176));
        System.out.println(areEqualByThreeDecimalPlaces(3.0, 3.0));
        System.out.println(areEqualByThreeDecimalPlaces(-3.123, 3.123));
    }

    private static boolean areEqualByThreeDecimalPlaces(double num1, double num2)
    {
        double tempNum1 = num1 * 1000;
        double tempNum2 = num2 * 1000;
        int tempIntNum1 = (int) tempNum1;
        int tempIntNum2 = (int) tempNum2;

        if(tempIntNum1 == tempIntNum2) return true;
        else {
            return false;
        }

    }
}
