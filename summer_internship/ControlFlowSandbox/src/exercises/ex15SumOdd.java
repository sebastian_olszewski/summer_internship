package ControlFlowSandbox.src.exercises;

public class ex15SumOdd {
    public static void main(String[] args) {
        System.out.println(sumOdd(1, 100));
        System.out.println(sumOdd(-1, 100));
        System.out.println(sumOdd(100, 100));
        System.out.println(sumOdd(13, 13));
        System.out.println(sumOdd(100, -100));
        System.out.println(sumOdd(100, 1000));
    }

    private static boolean isOdd(int number)
    {
        if(number < 0 || number % 2 == 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    private static long sumOdd(int start, int end)
    {
        if(start <= end && start >= 0 && end >= 0)
        {
            long sum = 0;
            for(int i = start; i <= end; i++)
            {
                if(isOdd(i) == true)
                {
                    sum += i;

                }
            }
            return sum;
        }
       return -1;
    }
}
