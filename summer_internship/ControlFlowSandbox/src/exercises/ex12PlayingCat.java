package ControlFlowSandbox.src.exercises;

public class ex12PlayingCat {
    public static void main(String[] args) {
        System.out.println(isCatPlaing(true, 10));
        System.out.println(isCatPlaing(false, 36));
        System.out.println(isCatPlaing(false, 35));

    }

    private static boolean isCatPlaing(boolean summer, int temperature)
    {
        if(summer == false)
        {
            if(temperature >= 25 && temperature <= 35)
            {
                return true;
            }else return false;
        }else
        {
            if(temperature >= 25 && temperature <= 45)
            {
                return true;
            }else return false;
        }
    }
}
