package ControlFlowSandbox.src.exercises;

public class ex10MinutesToYearsAndDaysCalculator {
    public static void main(String[] args) {
        printYearsAndDays(525600);
        printYearsAndDays(1051200);
        printYearsAndDays(561600);
    }
    private static void printYearsAndDays(long minutes)
    {
        int years;
        int temp;
        int days;
        if(minutes < 0)
        {
            System.out.println("Invalid value");
        }else
        {
            years = (int) minutes / 525600;
            temp = years * 365;
            days = (int) (minutes / 1440) - temp;

            System.out.println(minutes + " min = " + years + " y and " + days + " d");

        }
    }
}
