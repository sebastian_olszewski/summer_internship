package ControlFlowSandbox.src.exercises;

public class ex21GreatestCommonDivisor {
    public static void main(String[] args) {
    System.out.println(getGreatestCommonDivisor(25, 15));
        System.out.println(getGreatestCommonDivisor(12, 30));
        System.out.println(getGreatestCommonDivisor(9, 18));
        System.out.println(getGreatestCommonDivisor(81, 153));

    }

    private static int getGreatestCommonDivisor(int first, int second)
    {
        if(first < 10 || second < 10)
        {
            return -1;
        }else
        {
            int temp1 = first;
            int temp2 = second;

            while (temp1 != temp2)
            {
                if (temp1 > temp2)
                {
                    temp1 -= temp2;
                }else
                {
                    temp2 -= temp1;
                }
            }
            return temp1;
        }
    }
}
