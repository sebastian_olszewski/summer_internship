package ControlFlowSandbox.src.exercises;

public class ex19LastDigitChecker {
    public static void main(String[] args) {
        System.out.println(hasTheSameLastDigit(41, 22, 71));
        System.out.println(hasTheSameLastDigit(23, 32, 42));
        System.out.println(hasTheSameLastDigit(9, 99, 999));

        System.out.println(isValid(10));
        System.out.println(isValid(468));
        System.out.println(isValid(1051));
    }

    private static boolean hasTheSameLastDigit(int num1, int num2, int num3)
    {
        if(num1 < 10 || num2 < 10 || num3 < 10 || num1 > 1000 || num2 > 1000 || num3 > 1000)
        {
            return false;
        }else if (num1 % 10 == num2 % 10 || num1 % 10 == num3 % 10 || num2 % 10 == num3 % 10)
        {
                return true;
        }else
        {
            return false;
        }

    }

    private static boolean isValid(int number)
    {
        if(number >= 10 && number <= 1000)
        {
            return true;
        }else {
            return false;
        }
    }
}
