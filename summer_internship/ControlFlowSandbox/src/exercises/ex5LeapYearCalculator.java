package ControlFlowSandbox.src.exercises;

public class ex5LeapYearCalculator {
    public static void main(String[] args) {
        isLeapYear(-1600);
        isLeapYear(1600);
        isLeapYear(2017);
        isLeapYear(2000);
    }

    private static void isLeapYear(int year)
    {
        if(year >= 0 && year <= 9999)
        {
            divisibleby4(year);
        }else
        {
            System.out.println("false");
        }
    }
    private static void divisibleby4(int year)
    {
        if(year % 4 == 0)
        {
            divisibleby100(year);
        }else
        {
            System.out.println("false");
        }
    }
    private static void divisibleby100(int year)
    {
        if(year % 100 == 0)
        {
            divisibleby400(year);
        }else
        {
            System.out.println("false");
        }
    }
    private static void divisibleby400(int year)
    {
        if(year % 400 == 0)
        {
            System.out.println("This is a lap year");
        }else
        {
            System.out.println("false");
        }
    }
}
