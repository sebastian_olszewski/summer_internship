package ControlFlowSandbox.src.exercises;

public class ex16FirstAndLastDigitSum {
    public static void main(String[] args) {
        sumFirstAndLastDigit(252);
        sumFirstAndLastDigit(257);
        sumFirstAndLastDigit(0);
        sumFirstAndLastDigit(5);
        sumFirstAndLastDigit(-10);
    }
    private static void sumFirstAndLastDigit(int number) {
        if (number < 0) {
            System.out.println("-1");
        } else {
            int temp = number;
            while (temp >= 10) {
                temp /= 10;
            }
            System.out.println((number % 10) + temp);
        }
    }
}
