package ControlFlowSandbox.src.exercises;

public class ex17EvenDigitSum {
    public static void main(String[] args) {
        getEvenDigitSum(123456789);
        getEvenDigitSum(252);
        getEvenDigitSum(-22);

    }

    private static void getEvenDigitSum(int number)
    {
        int temp = number;
        int sum = 0;
        if(number < 0)
        {
            System.out.println("-1");
        }else
        {
            while (temp >= 10) {
                if (temp % 2 == 0)
                {
                    sum += temp % 10;
                    temp /= 10;
                }else
                {
                    temp /= 10;
                }
            }
            System.out.println(sum);
        }

    }
}
