package ControlFlowSandbox.src.main.resources;

public class ReferenceVsPrimitiveSandbox {
    public static void main(String[] args) {
        //Primitive types
        int n1 = 5;
        int n2 = n1;
        n1 = 6;


        //2 int vars (1rst init with literal, 2nd = 1rst) -> sout both -> change 1rst -> sout both
        //Reference types
        System.out.println(n1);


    }
}
