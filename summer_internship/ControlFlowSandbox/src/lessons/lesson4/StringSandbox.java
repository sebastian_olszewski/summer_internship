package ControlFlowSandbox.src.lessons.lesson4;

public class StringSandbox {
    public static void main(String[] args) {

        String greeting = new String("Hello");
        String greeting2 = new String("Hello");
        String greeting3 = new String("Czesc");
        System.out.println(greeting);

        int n1 = 5_000_000;
        System.out.println(greeting.equals(greeting2));
        System.out.println(greeting.equals(greeting3));

    }

}
